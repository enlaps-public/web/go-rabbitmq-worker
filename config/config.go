package config

type ConfigDatas struct {
	RabbitMQURI string
}

var Config *ConfigDatas

func InitializeConfiguration(rmqURI string) {
	Config = &ConfigDatas{
		RabbitMQURI: rmqURI,
	}
}
