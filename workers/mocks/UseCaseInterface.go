// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// UseCaseInterface is an autogenerated mock type for the UseCaseInterface type
type UseCaseInterface struct {
	mock.Mock
}

// Process provides a mock function with given fields: body
func (_m *UseCaseInterface) Process(body []byte) bool {
	ret := _m.Called(body)

	var r0 bool
	if rf, ok := ret.Get(0).(func([]byte) bool); ok {
		r0 = rf(body)
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}
