package workers

import (
	"context"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/log"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/rabbitmq"
	"time"
)

const maxRetryCount = 10

type UseCaseInterface interface {
	Process(body []byte) bool
}

type Worker struct {
	Logger          log.Logger
	QueueParameters rabbitmq.QueueParameters
	RetryCount      int
	UseCase         UseCaseInterface
	UseCaseName     string
}

func (w *Worker) info(msg string) {
	w.Logger.Info(fmt.Sprintf("Usecase %s, %s", w.UseCaseName, msg))
}

func (w *Worker) error(msg string) {
	w.Logger.Error(fmt.Sprintf("Usecase %s, %s", w.UseCaseName, msg))
}

func (w *Worker) debug(msg string) {
	w.Logger.Debug(fmt.Sprintf("Usecase %s, %s", w.UseCaseName, msg))
}

func (w *Worker) newChannel() (*amqp.Channel, error) {
	return rabbitmq.GetState().GetChannel(w.QueueParameters.PrefetchCount)
}

func (w *Worker) consume(ch *amqp.Channel) (<-chan amqp.Delivery, error) {
	return ch.Consume(
		w.QueueParameters.Name,
		w.QueueParameters.Consumer,
		w.QueueParameters.AutoAck,
		w.QueueParameters.Exclusive,
		w.QueueParameters.NoLocal,
		w.QueueParameters.NoWait,
		w.QueueParameters.Args,
	)
}

func (w *Worker) Start(ctx context.Context, workerChan chan *Worker) {
	defer func() {
		if r := recover(); r != nil {
			w.error(fmt.Sprintf("panic recovering %s", r))
			w.handleRecover(workerChan)
		}
	}()

	ch, err := w.newChannel()
	if err != nil {
		w.error(fmt.Sprintf("error when creating channel: %s", err.Error()))
		return
	}
	defer func(ch *amqp.Channel) {
		if err := ch.Close(); err != nil {
			w.info(fmt.Sprintf("error when closing channel: %s", err.Error()))
		}
	}(ch)

	messages, err := w.consume(ch)
	if err != nil {
		w.error("error when consuming channel")
		panic(err)
	}
	for {
		select {
		case <-ctx.Done():
			w.info(fmt.Sprintf("context cancelled %s, returning...", w.UseCaseName))
			return
		case message := <-messages:
			go w.processMessage(&rabbitmq.MessageWrapper{Message: &message})
		default:
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func (w *Worker) processMessage(message rabbitmq.MessageHandler) {
	w.info("starting to process message")
	if w.useCaseProcess(message.Body()) {
		w.info("Ack message")
		if err := message.Ack(false); err != nil {
			w.error(fmt.Sprintf("error when ack message: %s", err.Error()))
		}
	} else {
		w.info("Nack message")
		if err := message.Nack(false, false); err != nil {
			w.error(fmt.Sprintf("error when ack message: %s", err.Error()))
		}
	}
	w.resetRetryCount()
	return
}

func (w *Worker) useCaseProcess(body []byte) bool {
	defer func() bool {
		if err := recover(); err != nil {
			w.error(fmt.Sprintf("panic recovering when calling callback %s", err))
		}
		return false
	}()

	return w.UseCase.Process(body)
}

func (w *Worker) handleRecover(workerChan chan *Worker) {
	if w.RetryCount < maxRetryCount {
		retryTime := w.getRetryTime()
		w.info(fmt.Sprintf("sleeping %s", retryTime.String()))
		time.Sleep(retryTime)
		w.RetryCount++
		workerChan <- w
	} else {
		w.error("too many retries, stopping worker")
	}
}

func (w *Worker) resetRetryCount() {
	if w.RetryCount != 0 {
		w.RetryCount = 0
	}
}

func (w *Worker) getRetryTime() time.Duration {
	if w.RetryCount < 5 {
		return time.Minute
	} else if w.RetryCount < 8 {
		return 5 * time.Minute
	} else {
		return 10 * time.Minute
	}
}
