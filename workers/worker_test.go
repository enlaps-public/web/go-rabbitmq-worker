package workers

import (
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	mockLogger "gitlab.com/enlaps-public/web/go-rabbitmq-worker/log/mocks"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/rabbitmq"
	mockRMQ "gitlab.com/enlaps-public/web/go-rabbitmq-worker/rabbitmq/mocks"
	mockWorker "gitlab.com/enlaps-public/web/go-rabbitmq-worker/workers/mocks"
	"testing"
)

type WorkerSuite struct {
	suite.Suite
	Logger          *mockLogger.Logger
	Message         *mockRMQ.MessageHandler
	QueueParameters rabbitmq.QueueParameters
	UseCase         *mockWorker.UseCaseInterface
	Worker          *Worker
}

func (s *WorkerSuite) SetupSuite() {
	s.initNewTest()
}

func (s *WorkerSuite) initNewTest() {
	s.Logger = &mockLogger.Logger{}
	s.Message = &mockRMQ.MessageHandler{}
	s.QueueParameters = getQueueParameters()
	s.UseCase = &mockWorker.UseCaseInterface{}
	s.Worker = s.getWorker()
	s.mockInfoLogging()
	s.mockErrorLogging()
}

func TestWorkerSuite(t *testing.T) {
	suite.Run(t, new(WorkerSuite))
}

func (s *WorkerSuite) TestProcessMessageAckSuccess() {
	s.Worker.RetryCount = 5
	messageBody := []byte("mock_message")
	s.Message.On("Body").Return(
		func() []byte {
			return messageBody
		},
	).Once()
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return true
		},
	).Once()
	s.Message.On("Ack", false).Return(
		func(multiple bool) error {
			return nil
		},
	).Once()
	s.Worker.processMessage(s.Message)
	s.Require().Equal(0, s.Worker.RetryCount)
}

func (s *WorkerSuite) TestProcessMessageAckWithError() {
	messageBody := []byte("mock_message")
	s.Message.On("Body").Return(
		func() []byte {
			return messageBody
		},
	).Once()
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return true
		},
	).Once()
	s.Message.On("Ack", false).Return(
		func(multiple bool) error {
			return fmt.Errorf("ack error")
		},
	).Once()
	s.Worker.processMessage(s.Message)
}

func (s *WorkerSuite) TestProcessMessageNackSuccess() {
	messageBody := []byte("mock_message")
	s.Message.On("Body").Return(
		func() []byte {
			return messageBody
		},
	).Once()
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return false
		},
	).Once()
	s.Message.On("Nack", false, false).Return(
		func(multiple bool, requeue bool) error {
			return nil
		},
	).Once()
	s.Worker.processMessage(s.Message)
}

func (s *WorkerSuite) TestProcessMessageNackWithError() {
	messageBody := []byte("mock_message")
	s.Message.On("Body").Return(
		func() []byte {
			return messageBody
		},
	).Once()
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return false
		},
	).Once()
	s.Message.On("Nack", false, false).Return(
		func(multiple bool, requeue bool) error {
			return fmt.Errorf("nack error")
		},
	).Once()
	s.Worker.processMessage(s.Message)
}

func (s *WorkerSuite) TestUseCaseProcessSuccess() {
	messageBody := []byte("mock_message")
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return true
		},
	).Once()
	success := s.Worker.useCaseProcess(messageBody)
	s.Require().Equal(true, success)
}

func (s *WorkerSuite) TestUseCaseProcessFailure() {
	messageBody := []byte("mock_message")
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			return false
		},
	).Once()
	success := s.Worker.useCaseProcess(messageBody)
	s.Require().Equal(false, success)
}

func (s *WorkerSuite) TestUseCaseProcessPanic() {
	messageBody := []byte("mock_message")
	s.UseCase.On("Process", messageBody).Return(
		func(body []byte) bool {
			panic("panicking")
		},
	).Once()
	success := s.Worker.useCaseProcess(messageBody)
	s.Require().Equal(false, success)
}

func (s *WorkerSuite) getWorker() *Worker {
	return &Worker{
		Logger:          s.Logger,
		QueueParameters: s.QueueParameters,
		RetryCount:      0,
		UseCase:         s.UseCase,
		UseCaseName:     "mockUseCaseName",
	}
}

func getQueueParameters() rabbitmq.QueueParameters {
	return rabbitmq.QueueParameters{
		Name:          "",
		PrefetchCount: 2,
		Consumer:      "",
		AutoAck:       false,
		Exclusive:     false,
		NoLocal:       false,
		NoWait:        false,
		Args:          nil,
	}
}

func (s *WorkerSuite) mockInfoLogging() {
	s.Logger.On("Info", mock.AnythingOfType("string"))
}

func (s *WorkerSuite) mockErrorLogging() {
	s.Logger.On("Error", mock.AnythingOfType("string"))
}
