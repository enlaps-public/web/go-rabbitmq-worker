# Go RabbitMQ worker

Go RabbitMQ worker is a worker that consumes messages from RabbitMQ message brokers.

## Principle

The Go RabbitMQ worker consumes and processes messages from multiple queues.
A concurrency value can be set for each queue, resulting in multiple goroutines consuming the same queue.
A channel is created for each goroutine consuming a queue, therefore a prefetch count value (https://www.rabbitmq.com/consumer-prefetch.html) can also be set, resulting in parallel message consuming.
The queue parameters are customizable, as are the message processes.
The message is acknowledged if the process succeeds, unacknowledged otherwise.

## Installation

```
go get https://gitlab.com/enlaps-public/web/go-rabbitmq-worker
```

## Usage

### Configuration

Create a `RabbitMQWorker` struct with the appropriate configuration:

```
rabbitmqWorker := rmqw.RabbitMQWorker{
    Logger: myLogger,
    WorkersParameters: []rmqw.WorkerParameters{
        {
            UseCaseName: "my first use case",
            Concurrency: 1,
            UseCase: myFirstUseCaseInterface,
            QueueParameters: rabbitmq.QueueParameters{
                Name:          "my first queue name",
                PrefetchCount: 5,
                Consumer:      "first consumer name",
                AutoAck:       false,
                Exclusive:     false,
                NoLocal:       false,
                NoWait:        false,
                Args:          nil,
            },
        },
        {
            UseCaseName: "my second use case",
            Concurrency: 10,
            UseCase: mySecondUseCaseInterface,
            QueueParameters: rabbitmq.QueueParameters{
                Name:          "my first queue name",
                PrefetchCount: 2,
                Consumer:      "second consumer name",
                AutoAck:       false,
                Exclusive:     false,
                NoLocal:       false,
                NoWait:        false,
                Args:          nil,
            },
        },
	}
```

The Logger should fullfill the LoggerInterface contract:
```
Info(message string)
Error(message string)
Debug(message string)
```

All `UseCase` should fullfill the UseCaseInterface contract:
```
Process([]byte) bool
```

### Running

Run the worker:
```
rabbitmqWorker.Start(context, rabbitMQURI)
```

The `context` parameter should a cancellable context. The Go RabbitMQ worker will run until the context is cancelled.
The `rabbitMQURI` is the RabbitMQ URI scheme (https://www.rabbitmq.com/uri-spec.html)
