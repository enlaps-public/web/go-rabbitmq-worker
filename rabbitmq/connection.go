package rabbitmq

import (
	"context"
	"fmt"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/log"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/config"
)

type State struct {
	cancel     context.CancelFunc
	connection *amqp.Connection
	errors     chan *amqp.Error
	logger     log.Logger
}

type QueueParameters struct {
	Name          string
	PrefetchCount int
	Consumer      string
	AutoAck       bool
	Exclusive     bool
	NoLocal       bool
	NoWait        bool
	Args          amqp.Table
}

type Consumer interface {
	Consume(queue string, consumer string, autoAck bool, exclusive bool, noLocal bool, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error)
}
type Publisher interface {
	Publish(exchange string, key string, mandatory bool, immediate bool, msg amqp.Publishing) error
}

var ExternalPublisher = &amqp.Channel{}
var rabbitMQState *State = nil

func (s *State) GetChannel(prefetchCount int) (*amqp.Channel, error) {
	ch, err := s.connection.Channel()
	if err != nil {
		return nil, err
	}
	err = ch.Qos(prefetchCount, 0, false)
	if err != nil {
		return nil, err
	}
	return ch, nil
}

func GetState() *State {
	if rabbitMQState != nil {
		return rabbitMQState
	}
	rabbitMQState = &State{}
	return rabbitMQState
}

func (s *State) Build(logger log.Logger, cancel context.CancelFunc) *State {
	s.logger = logger
	s.cancel = cancel
	s.connectToRabbitMQ()
	s.setExternalPublisher()
	go s.handleConnectionErrors()
	return s
}

func (s *State) setExternalPublisher() {
	ch, err := s.GetChannel(1)
	if err != nil {
		s.logger.Error("error during external publisher channel")
	}
	*ExternalPublisher = *ch
}

func (s *State) connectToRabbitMQ() {
	s.logger.Info("connecting to rabbitmq")
	connection, err := amqp.Dial(config.Config.RabbitMQURI)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Connection to RabbitMQ failed: %s", err))
		panic(err)
	}
	s.connection = connection
	s.errors = connection.NotifyClose(make(chan *amqp.Error))
}

func (s *State) destroySingleton() {
	rabbitMQState = nil
	s.cancel()
}

func (s *State) handleConnectionErrors() {
	for {
		select {
		case <-s.errors:
			s.logger.Info("connection error, cancelling context")
			s.destroySingleton()
			return
		default:
			time.Sleep(time.Second)
		}
	}
}
