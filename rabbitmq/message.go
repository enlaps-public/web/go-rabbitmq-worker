package rabbitmq

import "github.com/streadway/amqp"

type MessageHandler interface {
	Ack(multiple bool) error
	Nack(multiple bool, requeue bool) error
	Body() []byte
}

type MessageWrapper struct {
	Message *amqp.Delivery
}

func (m *MessageWrapper) Ack(multiple bool) error {
	return m.Message.Ack(multiple)
}

func (m *MessageWrapper) Nack(multiple bool, requeue bool) error {
	return m.Message.Nack(multiple, requeue)
}

func (m *MessageWrapper) Body() []byte {
	return m.Message.Body
}
