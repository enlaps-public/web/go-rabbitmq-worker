package rabbitmq_worker

import (
	"context"
	"fmt"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/config"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/log"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/rabbitmq"
	"gitlab.com/enlaps-public/web/go-rabbitmq-worker/workers"
	"time"
)

type RabbitMQWorker struct {
	Logger            log.Logger
	WorkersParameters []WorkerParameters
	rabbitMQState     *rabbitmq.State
}

type WorkerParameters struct {
	UseCase         UseCaseInterface
	Concurrency     int
	QueueParameters rabbitmq.QueueParameters
	UseCaseName     string
}

type UseCaseInterface interface {
	Process([]byte) bool
}

func (r *RabbitMQWorker) Start(ctx context.Context, rmqURI string) {
	config.InitializeConfiguration(rmqURI)
	r.run(ctx)
	<-ctx.Done()
}

func (r *RabbitMQWorker) info(msg string) {
	r.Logger.Info(msg)
}

func (r *RabbitMQWorker) run(ctx context.Context) {
	defer r.restart(ctx)

	rmqCtx, cancel := context.WithCancel(ctx)
	r.withRabbitMQ(cancel)
	r.dispatch(rmqCtx)
}

func (r *RabbitMQWorker) restart(ctx context.Context) {
	r.info("restarting rabbitmq worker")
	r.run(ctx)
}

func (r *RabbitMQWorker) dispatch(ctx context.Context) {
	workerChannel := make(chan *workers.Worker)
	defer close(workerChannel)

	go r.consumeWorkers(ctx, workerChannel)
	for _, workerParams := range r.WorkersParameters {
		for i := 0; i < workerParams.Concurrency; i++ {
			r.info(fmt.Sprintf("starting worker %d for use case %s", i, workerParams.UseCaseName))
			workerChannel <- r.newWorker(workerParams)
		}
	}
	<-ctx.Done()
}

func (r *RabbitMQWorker) withRabbitMQ(cancel context.CancelFunc) {
	r.rabbitMQState = rabbitmq.GetState().Build(r.Logger, cancel)
}

func (r *RabbitMQWorker) newWorker(workerParams WorkerParameters) *workers.Worker {
	return &workers.Worker{
		Logger:          r.Logger,
		QueueParameters: workerParams.QueueParameters,
		RetryCount:      0,
		UseCase:         workerParams.UseCase,
		UseCaseName:     workerParams.UseCaseName,
	}
}

func (r *RabbitMQWorker) consumeWorkers(ctx context.Context, workerChannel chan *workers.Worker) {
	for {
		select {
		case <-ctx.Done():
			r.info("consumeWorkers: context done")
			return
		case w, ok := <-workerChannel:
			if ok {
				r.info(fmt.Sprintf("starting worker for use case %s", w.UseCaseName))
				go w.Start(ctx, workerChannel)
			}
		default:
			time.Sleep(time.Second)
		}
	}
}
